/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function
*/

	function addTwoNumbers(num1, num2){
		let sum = num1 + num2
		console.log("(sum) " + sum);

	};

	addTwoNumbers(1, 2);
	addTwoNumbers(3, 4);

	function subtractTwoNumbers(num3, num4){
		let subtract = num3 - num4
		console.log("(difference) " + subtract);

	};

	subtractTwoNumbers(2, 1);
	subtractTwoNumbers(8, 4);


/*
	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication. (use return statement)

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division. (use return statement)

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.


*/

	function multiplyNumbers(num5, num6){
		return num5 * num6
		
		
	};
	let multiplyNum = multiplyNumbers(2, 1)
		console.log(multiplyNum)



	function divideNumbers(num7, num8){
		return num7 / num8
		
		
	};
	let divideNum = divideNumbers(6, 2)
		console.log(divideNum)

/*
	3. 	Create a function which will be able to get total area of a circle from a provided radius.
			-a number should be provided as an argument.
			-formula: area = pi * (radius ** 2)
			-exponent operator **
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation. (use return statement)

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

		Log the value of the circleArea variable in the console.


*/
	function totalAreaOfCircle(radius){
		let cirleArea = 3.141592653589793238 * (radius **2);
		console.log("Total Area of the Circle " + cirleArea);

	};

	totalAreaOfCircle(3);
	
		

/*
	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-formula for average: num1 + num2 + num3 + ... divide by total numbers
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation. (use return statement) 

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
*/
	function averageFourNumbers(avenum1, avenum2, avenum3, avenum4){
		let averageVar = (avenum1 + avenum2 + avenum3 + avenum4)/4
		console.log("(Average of Four(4) Numbers) " + averageVar);

	};

	averageFourNumbers(6, 7, 9, 4);
	averageFourNumbers(4, 8, 9, 4);
	averageFourNumbers(5, 6, 5, 3);
	averageFourNumbers(8, 6, 8, 7);

/*



	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed. (use return statement)
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/


		function checkIfPassed(score){
			let passingRate = 75;
			let isPassed = score >= passingRate;
			let isFailed = score <= passingRate;


			console.log("Passed? " + isPassed);

		};

		checkIfPassed(75);


